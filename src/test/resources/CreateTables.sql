create sequence public.user_id_seq;

create sequence public.send_messages_id_seq;

create table public.users
(
    first_name   varchar(255),
    phone_number varchar(255),
    id           bigint default nextval('user_id_seq'::regclass) not null
);

alter sequence public.user_id_seq owned by public.users.id;

create table public.button_presses
(
    id        serial,
    date_time timestamp
);


create table public.message
(
    id              bigserial,
    text            varchar(255),
    type            smallint,
    receiver_type   varchar(255),
    not_before      time,
    not_after       time,
    times_sent      integer default 0     not null,
    media           text,
    next_message    bigint,
    max_amount      integer default 1     not null,
    exact_date_time timestamp,
    priority        boolean default false not null
);

create table public.send_message
(
    id           bigint default nextval('send_messages_id_seq'::regclass) not null,
    message_type smallint,
    message_id   bigint,
    time_send    timestamp
);

alter sequence public.send_messages_id_seq owned by public.send_message.id;


create sequence public.highscore_id_seq;

create table public.highscore
(
    id           bigint default nextval('highscore_id_seq'::regclass) not null,
    type smallint,
    highscore_value int
);

alter sequence public.highscore_id_seq owned by public.send_message.id;


