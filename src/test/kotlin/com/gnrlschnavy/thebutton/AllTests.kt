package com.gnrlschnavy.thebutton

import com.gnrlschnavy.thebutton.repository.MessageRepo
import com.gnrlschnavy.thebutton.service.MessageService
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.jdbc.Sql
import org.springframework.test.context.jdbc.SqlGroup


@SqlGroup(
    Sql(
        scripts = [
//            "/CreateTables.sql",
            "/InsertMessages.sql"
                  ],
        executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD
    ),
//    Sql(scripts = ["/EmptyDB.sql"], executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
)

class AllTests() {


    @SpringBootTest
    @Nested

    inner class TheButtonApplicationTests(
        @Autowired val messageRepo: MessageRepo,
        @Autowired val messageService: MessageService

    ) {

        @Test
        fun shouldSendGroupMessage() {
            val photoMessage = messageRepo.findById(8).get()
            messageService.determineMessageType(photoMessage)
        }

    }

}
