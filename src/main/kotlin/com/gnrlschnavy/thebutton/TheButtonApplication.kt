@file:Suppress("INLINE_FROM_HIGHER_PLATFORM")

package com.gnrlschnavy.thebutton

import com.gnrlschnavy.thebutton.domain.Button
import com.gnrlschnavy.thebutton.domain.jpa.ButtonPresses
import com.gnrlschnavy.thebutton.listener.ButtonListener
import com.gnrlschnavy.thebutton.repository.ButtonPressesRepo
import com.pi4j.Pi4J
import com.pi4j.context.Context
import com.pi4j.io.gpio.digital.DigitalInput
import com.pi4j.io.gpio.digital.PullResistance
import com.pi4j.platform.Platforms
import jakarta.annotation.PostConstruct
import jakarta.annotation.PreDestroy
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.scheduling.annotation.EnableScheduling
import java.net.Inet4Address
import java.nio.file.Files
import java.nio.file.Paths
import java.time.LocalDateTime

fun isRunningInDocker(): Boolean {
    // Check for .dockerenv file
    val dockerEnvPath = Paths.get("/.dockerenv")
    if (Files.exists(dockerEnvPath)) return true

    // Check for Docker-specific cgroup
    return try {
        Files.readAllLines(Paths.get("/proc/self/cgroup")).any { line -> "docker" in line }
    } catch (e: Exception) {
        false
    }
}

fun isRunningOnWSL(): Boolean {
    return try {
        Files.readAllLines(Paths.get("/proc/version")).any { line ->
            "microsoft" in line.lowercase() || "wsl" in line.lowercase()
        }
    } catch (e: Exception) {
        false
    }
}

fun isRunningOnMacOS(): Boolean {
    return System.getProperty("os.name").lowercase().contains("mac") || System.getProperty("os.name").lowercase()
        .contains("darwin")
}

fun isRunningOnRaspberryPi(): Boolean {
    return try {
        Files.readAllLines(Paths.get("/proc/cpuinfo")).any {
            "raspberry pi" in it.lowercase()
        }
    } catch (e: Exception) {
        false
    }
}


@SpringBootApplication
@EnableScheduling
class TheButtonApplication(
    private val buttonPressesRepo: ButtonPressesRepo,
    private val buttonListener: ButtonListener,
) {
    val pi4j: Context = Pi4J.newAutoContext()


    @PostConstruct
    fun initializeGpio() {

        val isDocker = isRunningInDocker()
        val isWSL = isRunningOnWSL()
        val isMacOS = isRunningOnMacOS()
        val isRaspberryPi = isRunningOnRaspberryPi()
        print("\n------>    ").apply {
            when {
                isDocker && isWSL -> println("Running in a Docker container on WSL \n").apply { return }
                isDocker && isMacOS -> println("Running in a Docker container on macOS\n").apply { return }
                isDocker -> println("Running in a Docker container, but not on WSL or macOS\n").apply { return }
                isWSL -> println("Running on WSL but not in a Docker container\n").apply { return }
                isMacOS -> println("Running on macOS but not in a Docker container\n").apply { return }
                isRaspberryPi -> println("Running on raspi so applying GPIO and Pi4J\n")
                else -> println("Not running in a Docker container or a known OS environment\n").apply { return }
            }
            print(" <-------")
        }


        println(Inet4Address.getLocalHost().hostAddress);
        Button.totalCount = buttonPressesRepo.countAllBy().toInt()
        buttonPressesRepo.save(ButtonPresses(-1, LocalDateTime.now()))

        println("Initializing GPIO")
        val platforms: Platforms = pi4j.platforms()
        println("Pi4J PLATFORMS")
        platforms.describe().print(System.out)
        val PIN_BUTTON = 24 // PIN 18 = BCM 24
        val buttonConfig = DigitalInput.newConfigBuilder(pi4j)
            .id("button")
            .name("Press button")
            .address(PIN_BUTTON)
            .pull(PullResistance.PULL_DOWN)
            .debounce(3000L)
            .provider("pigpio-digital-input")
        val button = pi4j.create(buttonConfig)
        button.addListener(buttonListener)
    }

    @PreDestroy
    fun shutdownGpio() {
        pi4j.shutdown()
    }
}

fun main(args: Array<String>) {
    runApplication<TheButtonApplication>(*args)
}
