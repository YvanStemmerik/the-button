package com.gnrlschnavy.thebutton.service

import com.gnrlschnavy.thebutton.domain.HighScoreType
import com.gnrlschnavy.thebutton.domain.MessageType
import com.gnrlschnavy.thebutton.event.ButtonPressEvent
import com.gnrlschnavy.thebutton.event.ButtonReleaseEvent
import com.gnrlschnavy.thebutton.repository.MessageRepo
import org.slf4j.LoggerFactory
import org.springframework.context.event.EventListener
import org.springframework.stereotype.Service

@Service
class LongPressService(
    private val highScoreService: HighScoreService,
    private val messageService: MessageService,
    private val messageRepo: MessageRepo,
) {
    private val logger = LoggerFactory.getLogger(this::class.java)
    private var buttonPressed = false
    private var startTime = System.nanoTime() / 1000000

    @EventListener(ButtonPressEvent::class)
    fun buttonPressedEvent() {
        if (buttonPressed) {
            logger.info("Something went wrong, button was already pressed!")
        } else {
            buttonPressed = true
            startTime = System.nanoTime() / 1000000
        }
    }

    @EventListener(ButtonReleaseEvent::class)
    fun buttonReleasedEvent() {
        val now = System.nanoTime() / 1000000
        val pressTime = (now - startTime) / 1000
        if (highScoreService.isHighestScore(pressTime.toInt(), HighScoreType.LONGPRESS)) {
            highScoreService.addToHighScore(pressTime.toInt(), HighScoreType.LONGPRESS)
            val message = messageRepo.findMessageByType(MessageType.LONGPRESS)
                .filter { it.timesSent < it.maxAmount }
                .random()
            messageService.determineMessageType(message, mapOf("*longpress*" to pressTime.toString()))
        }
        buttonPressed = false
    }

}
