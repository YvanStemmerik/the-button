package com.gnrlschnavy.thebutton.service

import com.gnrlschnavy.thebutton.domain.Button
import com.gnrlschnavy.thebutton.domain.Group
import com.gnrlschnavy.thebutton.domain.MediaMessageDTO
import com.gnrlschnavy.thebutton.domain.MessageDTO
import com.gnrlschnavy.thebutton.domain.MessageGroups
import com.gnrlschnavy.thebutton.domain.MessageType
import com.gnrlschnavy.thebutton.domain.jpa.Message
import com.gnrlschnavy.thebutton.domain.jpa.SendMessage
import com.gnrlschnavy.thebutton.domain.jpa.Users
import com.gnrlschnavy.thebutton.handler.OkRequestHandler
import com.gnrlschnavy.thebutton.repository.MessageRepo
import com.gnrlschnavy.thebutton.repository.SendMessageRepo
import com.gnrlschnavy.thebutton.repository.UserRepo
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service
import java.time.LocalDateTime
import java.time.LocalTime
import java.util.LinkedList
import java.util.Queue

@Service
class MessageService(
    private val okRequestHandler: OkRequestHandler,
    private val messageRepo: MessageRepo,
    private val usersRepo: UserRepo,
    private val sendMessageRepo: SendMessageRepo,
) {

    private val logger = LoggerFactory.getLogger(this::class.java)

    @Value("\${button.productId}")
    private var productID = ""

    @Value("\${button.phoneId}")
    private var phoneId = ""

    val priorityQueue: Queue<Message> = LinkedList()

    fun determineMessage() {

        if (Button.shouldSendMessage()) {
            val messages = messageRepo.findAll()
                .filter { it.timesSent < it.maxAmount }
                .filter {
                    (it.type == MessageType.PHOTO
                            || it.type == MessageType.STORY
                            || it.type == MessageType.ASSIGNMENT)
                            && it.timesSent < it.maxAmount
                }
            val sendMessages = sendMessageRepo.findAllByTimeSendAfter(LocalDateTime.now().minusMinutes(180))
            val nextMessage = when {
                priorityQueue.isNotEmpty() -> { removeFromPriorityQueue() }
                noAssignmentMessagesSendAndInQueue(sendMessages) -> messages.filter { it.type == MessageType.ASSIGNMENT }.random()
                noPhotoSend(sendMessages) -> messages.filter { it.type == MessageType.PHOTO }.random()
                else -> null
            }
            if (nextMessage != null) {
                determineMessageType(nextMessage)
            }
        }
    }

    private fun removeFromPriorityQueue(): Message {
        val message = priorityQueue.remove()
        logger.info("Removing message from priority queue ${message.type}")
        return message
    }

    private fun noPhotoSend(sendMessages: List<SendMessage>): Boolean {
        val messagesSend = sendMessages.any { it.messageType == MessageType.PHOTO }
        val messagesInQueue = priorityQueue.any { it.type == MessageType.PHOTO }
        return !messagesSend && !messagesInQueue
    }

    private fun noAssignmentMessagesSendAndInQueue(sendMessages: List<SendMessage>): Boolean {
        val range = 45..75
        val randomRange = range.shuffled().first()
        val messagesSend = sendMessages.any {
            it.messageType == MessageType.ASSIGNMENT &&
                    it.timeSend.isAfter(LocalDateTime.now().minusMinutes(randomRange.toLong()))
        }
        val messagesInQueue = priorityQueue.any { it.type == MessageType.ASSIGNMENT }
        return !messagesSend && !messagesInQueue
    }

    fun determineMessageType(
        message: Message,
        replacements: Map<String, String> = emptyMap(),
        updateCounters: Boolean = true,
    ) {
        if (messageNotInTimeSlot(message)) {
            return
        }
        when (message.type) {
            MessageType.PHOTO -> sendGroupMessageMedia(message, replacements)
            MessageType.SCHEDULED -> sendGroupMessage(message, replacements)
            MessageType.NOTPRESSEDINAWHILE -> sendGroupMessage(message, replacements)
            MessageType.AANKLEDING -> sendMessageToSpecificGroup(message, MessageGroups.AANKLEDING)
            MessageType.STORY -> sendGroupMessage(message, replacements)
            MessageType.MANYPRESSES -> sendGroupMessage(message, replacements)
            MessageType.ASSIGNMENT -> determineReceiverType(message)
            MessageType.LONGPRESS -> sendGroupMessage(message, replacements)
            MessageType.SECRETCODE -> { sendGroupMessage(message, replacements) }
            MessageType.UNKNOWN -> {}
        }
        updateMessageStats(message)
        if(updateCounters) {
            updateCounters(message)
        }
    }

    fun messageNotInTimeSlot(message: Message): Boolean {
        message.notAfter?.let {
            if (LocalTime.now().isAfter(it)) {
                return true
            }
        }
        message.notBefore?.let {
            if (LocalTime.now().isBefore(it)) {
                return true
            }
        }
        return false
    }

    fun determineReceiverType(message: Message) {
        when {
            message.receiverType.contains("GROUP") -> checkExtraGroupParams(message)
            message.receiverType.contains("RANDOM") -> sendMessageToRandom(message)
        }
    }

    fun checkExtraGroupParams(message: Message) {
       when {
           message.receiverType == "GROUP" -> sendGroupMessage(message)
           message.receiverType.contains("*") -> sendGroupMessageWithUserNames(message)
       }

    }

    private fun sendGroupMessageWithUserNames(message: Message) {
        val amount = message.receiverType.split("*").last().toInt()
        val shuffledUsers = usersRepo.findAll().shuffled()
        val randomUserNames = shuffledUsers.take(amount)
        val replacements: MutableMap<String, String> = mutableMapOf()
        randomUserNames.forEachIndexed { index, user -> replacements["*name${index+1}*"] = user.firstName }
        sendGroupMessage(message, replacements)
    }

    fun sendMessageToRandom(message: Message) {
        val action = message.receiverType.split("*").last()
        when {
            action.contains("NEUZEN") -> neuzenAction(message, action)
            action.contains("-") -> minusAction(action, message)
            else -> randomAction(action, message)
        }
    }

    private fun neuzenAction(message: Message, action: String) {
        val amount = action.split("-").last().toInt()
        val shuffledUsers = usersRepo.findAll().shuffled()
        val firstMessageBatch = shuffledUsers.take(shuffledUsers.size - amount)
        val secondMessageBatch = shuffledUsers.takeLast(amount)
        val replacements: MutableMap<String, String> = mutableMapOf()
        secondMessageBatch.forEachIndexed { index, user -> replacements["*name${index+1}*"] = user.firstName }
        sendMessageToUsers(firstMessageBatch, message, replacements)
    }

    fun minusAction(action: String, message: Message) {
        val shuffledUsers = usersRepo.findAll().shuffled()
        val firstMessageBatch = shuffledUsers.take(shuffledUsers.size - action.replace("-", "").toInt())
        val secondMessageBatch = shuffledUsers.takeLast(action.replace("-", "").toInt())
        sendMessageToUsers(firstMessageBatch, message)
        if (message.nextMessage != null) {
            val nextMessage = messageRepo.findById(message.nextMessage).get()
            sendMessageToUsers(secondMessageBatch, nextMessage)
        }
    }

    fun randomAction(action: String, message: Message) {
        val shuffledUsers = usersRepo.findAll().shuffled()
        val batch = shuffledUsers.take(action.toInt())
        sendMessageToUsers(batch, message)
    }

    fun sendMessageToSpecificGroup(message: Message, group: MessageGroups) {
        val groups = Group().getGroups()
        when (group) {
            MessageGroups.AANKLEDING -> sendMessageToUsers(groups[MessageGroups.AANKLEDING]!!, message)
        }
    }

    fun findMessageByType(type: MessageType): List<Message> {
        return messageRepo.findMessageByType(type)
    }

    fun doStringReplacements(
        message: Message,
        replacements: Map<String, String>? = mapOf(),
    ): String {
        var replacementMessage = message
        replacements?.forEach {
            replacementMessage = replacementMessage.copy(text = replacementMessage.text.replace(it.key, it.value))
        }
        return replacementMessage.text
    }

    fun sendGroupMessage(message: Message, replacements: Map<String, String> = emptyMap()) {
        val url = "https://api.maytapi.com/api/$productID/$phoneId/sendMessage"
        okRequestHandler.post(
            url = url,
            requestParams = emptyMap(),
            requestBody = MessageDTO(
                toNumber = "120363144175433501@g.us",
                message = doStringReplacements(message, replacements)
            ).toJsonString(),
            expectedReturnType = Any::class.java
        )
    }

    fun sendGroupMessageMedia(message: Message, replacements: Map<String, String> = emptyMap()) {
        val url = "https://api.maytapi.com/api/$productID/$phoneId/sendMessage"
        okRequestHandler.post(
            url = url,
            requestParams = emptyMap(),
            requestBody = MediaMessageDTO(
                toNumber = "120363144175433501@g.us",
                media = "data:image/png;base64," + message.media!!,
                message = doStringReplacements(message, replacements)
            ).toJsonString(),
            expectedReturnType = Any::class.java
        )
    }

    fun sendMessageToUsers(userList: List<Users>, message: Message, replacements: Map<String, String> = emptyMap()) {
        val url = "https://api.maytapi.com/api/$productID/$phoneId/sendMessage"
        userList.forEach { user ->
            okRequestHandler.post(
                url = url,
                requestParams = emptyMap(),
                requestBody = MessageDTO(
                    toNumber = user.phoneNumber,
                    message = doStringReplacements(message, replacements + mapOf("*name*" to user.firstName))
                ).toJsonString(),
                expectedReturnType = Any::class.java
            )
        }
    }

    fun updateCounters(message: Message) {
        Button.timesPressedSinceLastMessage = 0
        Button.timeLastSendMessage = LocalDateTime.now()
    }

    private fun updateMessageStats(message: Message) {
        messageRepo.save(message.copy(timesSent = message.timesSent + 1))
        sendMessageRepo.save(
            SendMessage(
                id = -1,
                messageId = message.id,
                timeSend = LocalDateTime.now(),
                messageType = message.type
            )
        )
    }
}
