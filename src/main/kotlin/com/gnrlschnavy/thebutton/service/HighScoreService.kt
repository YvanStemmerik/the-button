package com.gnrlschnavy.thebutton.service

import com.gnrlschnavy.thebutton.domain.HighScoreType
import com.gnrlschnavy.thebutton.domain.jpa.Highscore
import com.gnrlschnavy.thebutton.repository.HighScoreRepo
import org.springframework.stereotype.Service

@Service
class HighScoreService(
    private val highScoreRepo: HighScoreRepo,
) {
    fun isHighestScore(score: Int, type: HighScoreType): Boolean {
        return score > (highScoreRepo.findAllByType(type).maxByOrNull { it.highscoreValue }?.highscoreValue ?: -1)
    }

    fun addToHighScore(pressTime: Int, type: HighScoreType) {
        highScoreRepo.save(Highscore(-1, type, pressTime))
    }

    fun getHighestScore(type: HighScoreType): Int  {
        return highScoreRepo.findAllByType(type).maxByOrNull { it.highscoreValue }?.highscoreValue ?: -1
    }
}
