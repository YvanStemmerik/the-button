package com.gnrlschnavy.thebutton.service

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.gnrlschnavy.thebutton.domain.FileInfo
import org.springframework.stereotype.Service
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter
import java.util.concurrent.CopyOnWriteArrayList

@Service
class SseService() {

    private val emitters = CopyOnWriteArrayList<SseEmitter>()

    fun addEmitter(emitter: SseEmitter) {
        emitters.add(emitter)
    }

    fun removeEmitter(emitter: SseEmitter) {
        emitters.remove(emitter)
    }

    suspend fun sendEventToClients(fileInfo: FileInfo) {
        val deadEmitters = mutableListOf<SseEmitter>()
        emitters.forEach { emitter ->
            try {
                val objectMapper = jacksonObjectMapper()
                val fileInfoJson = objectMapper.writeValueAsString(fileInfo)
                emitter.send(SseEmitter.event().data(fileInfoJson))
            } catch (e: Exception) {
                println(e.message)
                deadEmitters.add(emitter)
            }
        }
        emitters.removeAll(deadEmitters)
    }
}
