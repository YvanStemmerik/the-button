package com.gnrlschnavy.thebutton.service

import org.springframework.core.io.Resource
import org.springframework.core.io.UrlResource
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Service
import java.io.BufferedInputStream
import java.io.IOException
import java.io.InputStream
import javax.sound.sampled.AudioFormat
import javax.sound.sampled.AudioSystem
import javax.sound.sampled.DataLine
import javax.sound.sampled.SourceDataLine

@Service
class MusicService {

    fun playNormalSound() {
        playRandomSound("sounds", 5)
    }

    fun playWinningSound(secretCodeName: String) {
        playSpecificSound("knocks/$secretCodeName.wav")
    }

    private fun playRandomSound(folder: String, numberOfFiles: Int) {
        val randomGeneratedInt = (1..numberOfFiles).random()
        val classLoader = javaClass.classLoader
        val resource = classLoader.getResourceAsStream("$folder/$randomGeneratedInt.wav")!!
        val BUFFER_SIZE = 128000
        val bufferedIn: InputStream = BufferedInputStream(resource)
        val audioStream = AudioSystem.getAudioInputStream(bufferedIn)
        val audioFormat: AudioFormat = audioStream.format
        val info = DataLine.Info(SourceDataLine::class.java, audioFormat)
        val sourceLine = AudioSystem.getLine(info) as SourceDataLine
        sourceLine.open(audioFormat)
        sourceLine.start()
        var nBytesRead = 0
        val abData = ByteArray(BUFFER_SIZE)
        while (nBytesRead != -1) {
            try {
                nBytesRead = audioStream.read(abData, 0, abData.size)
            } catch (e: IOException) {
                e.printStackTrace()
            }
            if (nBytesRead >= 0) {
                sourceLine.write(abData, 0, nBytesRead)
            }
        }
        sourceLine.drain()
        sourceLine.close()
    }

    private fun playSpecificSound(filePath: String) {
        val classLoader = javaClass.classLoader
        val resource = classLoader.getResourceAsStream(filePath.replace(" ", ""))!!
        val BUFFER_SIZE = 128000
        val bufferedIn: InputStream = BufferedInputStream(resource)
        val audioStream = AudioSystem.getAudioInputStream(bufferedIn)
        val audioFormat: AudioFormat = audioStream.format
        val info = DataLine.Info(SourceDataLine::class.java, audioFormat)
        val sourceLine = AudioSystem.getLine(info) as SourceDataLine
        sourceLine.open(audioFormat)
        sourceLine.start()
        var nBytesRead = 0
        val abData = ByteArray(BUFFER_SIZE)
        while (nBytesRead != -1) {
            try {
                nBytesRead = audioStream.read(abData, 0, abData.size)
            } catch (e: IOException) {
                e.printStackTrace()
            }
            if (nBytesRead >= 0) {
                sourceLine.write(abData, 0, nBytesRead)
            }
        }
        sourceLine.drain()
        sourceLine.close()
    }

    fun streamRandomAudio(folder: String, numberOfFiles: Int): ResponseEntity<Resource?> {
        val randomGeneratedInt = (1..numberOfFiles).random()
        val filename = "$randomGeneratedInt.wav"
        val classLoader = javaClass.classLoader
        val resource: Resource =  UrlResource(classLoader.getResource("$folder/$filename")!!.toURI())

        if (!resource.exists()) {
            return ResponseEntity.notFound().build()
        }

        val contentType = when {
            filename.endsWith(".mp3") -> "audio/mp3"
            filename.endsWith(".wav") -> "audio/wav"
            else -> "application/octet-stream" // Default or unknown file type
        }

        return ResponseEntity.ok()
            .contentType(MediaType.parseMediaType(contentType))
            .header(HttpHeaders.CONTENT_DISPOSITION, "inline; filename=\"${resource.filename}\"") // Use "inline" to play directly in the browser
            .body(resource)
    }

    fun streamSpecificAudio(folder: String, filename: String): ResponseEntity<Resource?> {
        val classLoader = javaClass.classLoader
        val resource: Resource =  UrlResource(classLoader.getResource("$folder/${filename.replace(" ", "")}")!!.toURI())

        if (!resource.exists()) {
            return ResponseEntity.notFound().build()
        }

        val contentType = when {
            filename.endsWith(".mp3") -> "audio/mp3"
            filename.endsWith(".wav") -> "audio/wav"
            else -> "application/octet-stream" // Default or unknown file type
        }

        return ResponseEntity.ok()
            .contentType(MediaType.parseMediaType(contentType))
            .header(HttpHeaders.CONTENT_DISPOSITION, "inline; filename=\"${resource.filename}\"") // Use "inline" to play directly in the browser
            .body(resource)
    }

}
