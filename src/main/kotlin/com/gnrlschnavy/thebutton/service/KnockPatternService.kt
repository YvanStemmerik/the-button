package com.gnrlschnavy.thebutton.service

import com.gnrlschnavy.thebutton.domain.FileInfo
import com.gnrlschnavy.thebutton.event.ButtonPressEvent
import com.gnrlschnavy.thebutton.isRunningOnRaspberryPi
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.slf4j.LoggerFactory
import org.springframework.context.event.EventListener
import org.springframework.stereotype.Service
import java.util.*
import kotlin.concurrent.schedule
import kotlin.math.abs


@Service
class KnockPatternService(
    private val musicService: MusicService,
    private val sseService: SseService
) {

    private val logger = LoggerFactory.getLogger(KnockPatternService::class.java)

    private final val maximumKnocks = 20
    private final val rejectValue = 20
    private final val averageRejectValue = 40
    private val secretCodes = mapOf(
        "Shave and a Haircut" to intArrayOf(50, 25, 25, 50, 100, 50, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
        "SOS" to intArrayOf(50, 50, 50, 100, 100, 100, 50, 50, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
        "WE WILL ROCK YOU" to intArrayOf(50, 50, 100, 50, 50, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
        "Eye of the tiger" to intArrayOf(100, 50, 50, 100, 50, 50, 100, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
    )
    private var currentKnockNumber = 0
    private var startTime = System.nanoTime() / 1000000
    private var now = System.nanoTime() / 1000000
    private var knockReadings = IntArray(maximumKnocks) { -1 }
    private var listening = false
    private var timerRunning = false
    private lateinit var timer: TimerTask


    @EventListener(ButtonPressEvent::class)
    fun handleButtonPressEvent(
        event: ButtonPressEvent,
    ) {

        if (!listening) {  //start listening start timer -> first knock
            listening = true
            timerRunning = true
            startTime = System.nanoTime() / 1000000
            timer = Timer("SettingUp", false).schedule(2000) {
                logger.info("Timer past")
                timerRunning = false
                listening = false
                resetKnock()
            }
        } else {
            if (timerRunning) { //subsequent knocks restart timer, still listening and timer still running
                if(currentKnockNumber >= 20){
                    startCoroutineValidation()
                }else {
                    now = System.nanoTime() / 1000000
                    knockReadings[currentKnockNumber] = (now - startTime).toInt()
                    logger.info("Knock ${currentKnockNumber + 1} at ${now - startTime}")
                    currentKnockNumber++
                    startTime = now
                    now = System.nanoTime() / 1000000
                    timer.cancel()
                    timer = Timer("SettingUp", false).schedule(2000) {
                        logger.info("Timer past")
                        timerRunning = false
                        listening = false
                        startCoroutineValidation()
                    }
                }
                logger.info(knockReadings.contentToString())
            }
        }
    }

    private fun startCoroutineValidation() {
        CoroutineScope(Dispatchers.IO).launch {
            secretCodes.forEach { validateKnock(it.key, it.value) }
            resetKnock()
        }
    }

    private fun resetKnock() {
        listening = false
        timerRunning = false
        currentKnockNumber = 0
        (0 until maximumKnocks).forEach { i ->
            knockReadings[i] = -1
        }
        logger.info("Resetting knock")
    }

    private suspend fun validateKnock(secretCodeName: String, secretCode: IntArray): Boolean {
        var currentKnockCount = 0
        var secretKnockCount = 0
        var maxKnockInterval = 1

        (0 until maximumKnocks).forEach { i ->
            if (knockReadings[i] >= 0) {
                currentKnockCount++
            }
            if (knockReadings[i] >= 0) {
                secretKnockCount++
            }
            if (knockReadings[i] > maxKnockInterval) {
                maxKnockInterval = knockReadings[i]
            }
        }

        if (currentKnockCount != secretKnockCount) {
            return false
        }

        var totalTimeDifference = 0
        var timeDiff = 0;
        val tempKnockReadings = IntArray(maximumKnocks) { -1 }
        (0 until maximumKnocks).map { i ->
            tempKnockReadings[i] = scale(knockReadings[i], 0, maxKnockInterval, 0, 100)
            timeDiff = abs(tempKnockReadings[i] - secretCode[i])
            if (timeDiff > rejectValue) {
                return false
            }
            totalTimeDifference += timeDiff
        }
        if (totalTimeDifference / secretKnockCount > averageRejectValue) {
            return false
        }
//      only on pi, else do sse
        if(isRunningOnRaspberryPi()) {
            logger.info("Running on raspberry so using coroutines to make music")
            CoroutineScope(Dispatchers.IO).launch {
                musicService.playWinningSound(secretCodeName)
            }
        } else{
            sseService.sendEventToClients(FileInfo("knocks", "$secretCodeName.wav"))
        }
        return true
    }

    fun scale(value: Int, fromLow: Int, fromHigh: Int, toLow: Int, toHigh: Int): Int {
        return toLow + ((value - fromLow) * (toHigh - toLow)) / (fromHigh - fromLow)
    }

}
