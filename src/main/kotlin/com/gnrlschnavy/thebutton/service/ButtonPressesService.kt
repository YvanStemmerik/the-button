package com.gnrlschnavy.thebutton.service

import com.gnrlschnavy.thebutton.domain.jpa.ButtonPresses
import com.gnrlschnavy.thebutton.repository.ButtonPressesRepo
import org.springframework.stereotype.Service


@Service
class ButtonPressesService(private val buttonRepo: ButtonPressesRepo) {
    fun saveButtonPress(){
        buttonRepo.save(ButtonPresses())
    }
}
