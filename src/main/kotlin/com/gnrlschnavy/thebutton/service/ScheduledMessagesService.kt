package com.gnrlschnavy.thebutton.service

import com.gnrlschnavy.thebutton.domain.Button
import com.gnrlschnavy.thebutton.domain.HighScoreType
import com.gnrlschnavy.thebutton.domain.MessageType
import com.gnrlschnavy.thebutton.repository.ButtonPressesRepo
import com.gnrlschnavy.thebutton.repository.MessageRepo
import com.gnrlschnavy.thebutton.repository.SendMessageRepo
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Service
import java.time.LocalDateTime
import java.time.LocalTime
import java.time.temporal.ChronoUnit
import kotlin.random.Random

@Service
class ScheduledMessagesService(
    private val buttonPressesRepo: ButtonPressesRepo,
    private val messageRepo: MessageRepo,
    private val messageService: MessageService,
    private val sendMessageRepo: SendMessageRepo,
    private val highScoreService: HighScoreService,
    private val buttonPressService: ButtonPressesService
) {

    private val logger = LoggerFactory.getLogger(this::class.java)
    private var manyPressesHighScore: Int = highScoreService.getHighestScore(HighScoreType.MANYPRESS)

    @Value("\${button.max-in-10-seconds}")
    private var maxIn10Seconds: Int = 0

    @Value("\${button.time-not-pressed-in-minutes}")
    private var timeNotPressedInMinutes: Int = 0

    @Scheduled(fixedRate = 1000 * 60 * 120)
    fun sendStoryMessage() {
        val message = messageRepo.findMessageByType(MessageType.STORY)
            .sortedBy { it.id }
            .firstOrNull { it.timesSent < it.maxAmount }
            ?: run { logger.info("No story to add to priority queue"); return }
        val storyMessageBeenSendInLast3Hours = sendMessageRepo.findAllByMessageTypeAndTimeSendAfter(MessageType.STORY, LocalDateTime.now().minusHours(3)) != null
        if(messageService.priorityQueue.all { it.type != MessageType.STORY } && !storyMessageBeenSendInLast3Hours) {
            messageService.determineMessageType(message, emptyMap(), false)
        }
    }

    @Scheduled(fixedRate = 1000 * 10)
    fun checkIfPressedManyTimes() {
        val count = buttonPressesRepo.countAllByDateTimeAfter()
        if (count > manyPressesHighScore && count > maxIn10Seconds) {
            manyPressesHighScore = count.toInt()
            highScoreService.addToHighScore(count.toInt(), HighScoreType.MANYPRESS)
            val message = messageRepo.findMessageByType(MessageType.MANYPRESSES)
                .filter { it.timesSent < it.maxAmount }
                .random()
            messageService.determineMessageType(message, mapOf("*manypresses*" to count.toString()))
        }
    }

    @Scheduled(fixedRate = 1000 * 60)
    fun checkForScheduledMessage() {
        val scheduledMessages = messageService.findMessageByType(MessageType.SCHEDULED)
        scheduledMessages.forEach { message ->
            if (
                message.exactDateTime!!.isBefore(LocalDateTime.now().plusMinutes(1))
                && message.exactDateTime.isAfter(LocalDateTime.now().minusMinutes(1))
                && message.timesSent < message.maxAmount
            ) {
                logger.info("Sending scheduled message | messageId: ${message.id}")
                messageService.determineMessageType(message, emptyMap(), false)
            }
        }
    }

    @Scheduled(fixedRate = 1000 * 10, initialDelay = 1000 * 10)
    fun checkIfPressedInLastHour() {
        if (
            hasNotBeenPressedInAwhile() &&
            messageTypeHasNotBeenSentInPeriod(2 * timeNotPressedInMinutes, MessageType.NOTPRESSEDINAWHILE) &&
            between()
        ) {
            val timeBetweenInMinutes = ChronoUnit.MINUTES.between(Button.timeLastPressed, LocalDateTime.now())
            val message = messageRepo.findMessageByType(MessageType.NOTPRESSEDINAWHILE)
                .filter { it.timesSent < it.maxAmount }
                .random()
            logger.info("Sending message because button has not been pressed in the $timeBetweenInMinutes | messageId: ${message.id}")
            messageService.determineMessageType(message, mapOf("*minutes*" to timeBetweenInMinutes.toString()))
        }
    }

    //TODO YS: REMOVE BEFORE LOADING ON BUTTON
//    @Scheduled(fixedRate = 1000 * 60 * 10)
    fun pressButton(){
        val system = System.getProperty("os.name")
        if(system == "Mac OS X") {
            val randomNumber = Random.nextInt(1, 30).toLong()
            repeat((0..randomNumber).count()) {
                val randomMillies = Random.nextInt(100, 1000)
                messageService.determineMessage()
                buttonPressService.saveButtonPress()
//                Button.timeLastPressed = LocalDateTime.now()
                Thread.sleep(randomMillies.toLong())
            }
        }
    }

    private fun between(): Boolean {
        val now = LocalTime.now()
        val after = LocalTime.of(19,0)
        val before = LocalTime.of(12,0)
        return now.isBefore(after) && now.isAfter(before)
    }

    private fun messageTypeHasNotBeenSentInPeriod(threshold: Int, messageType: MessageType): Boolean {
        val after = LocalDateTime.now().minusMinutes(threshold.toLong())
        val message = sendMessageRepo.findAllByMessageTypeAndTimeSendAfter(messageType, after)
        return message == null
    }

    private fun hasNotBeenPressedInAwhile() =
        Button.timeLastPressed.isBefore(LocalDateTime.now().minusMinutes(timeNotPressedInMinutes.toLong()))



}
