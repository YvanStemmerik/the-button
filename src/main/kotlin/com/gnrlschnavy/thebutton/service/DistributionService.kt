package com.gnrlschnavy.thebutton.service

import org.springframework.stereotype.Service
import java.util.*
import kotlin.math.PI
import kotlin.math.exp
import kotlin.math.pow
import kotlin.math.sqrt

@Service
class DistributionService {

    data class ValueWithProbability(val value: Double, var probability: Double)

    fun pickValueFromDistribution(): Int {
        val random = Random()
        var cumulativeProbability = 0.0
        val randomValue = random.nextDouble()
        for (entry in normalDistribution) {
            cumulativeProbability += entry.probability
            if (randomValue <= cumulativeProbability) {
                return entry.value.toInt()
            }
        }
        // Fallback to the last value (to handle rounding errors)
        return normalDistribution.last().value.toInt()
    }

    companion object{

        private const val min = 5.0
        private const val max = 30.0
        private const val numSteps = 24
        private val random = Random()
        val normalDistribution = createNormalDistribution(min, max, numSteps, random)

        private fun createNormalDistribution(min: Double, max: Double, numSteps: Int, random: Random): List<ValueWithProbability> {
            require(min < max) { "Minimum value must be less than the maximum value." }
            require(numSteps > 1) { "Number of steps must be greater than 1." }

            val stepSize = (max - min) / (numSteps - 1)
            val mean = (max +1 + min) / 2.0
            val standardDeviation = (max - min) / 2.0 // Adjust this factor to control the spread of the distribution

            val distribution = mutableListOf<ValueWithProbability>()

            for (i in 0 until numSteps) {
                val x = min + i * stepSize
                val probability = calculateNormalDistributionProbability(x, mean, standardDeviation)
                distribution.add(ValueWithProbability(x, probability))
            }

            normalizeProbabilities(distribution)

            return distribution
        }


        private fun calculateNormalDistributionProbability(x: Double, mean: Double, standardDeviation: Double): Double {
            val coefficient = 1.0 / (standardDeviation * sqrt(2 * PI))
            val exponent = -0.5 * ((x - mean) / standardDeviation).pow(2)
            return coefficient * exp(exponent)
        }

        private fun normalizeProbabilities(distribution: MutableList<ValueWithProbability>) {
            val totalProbability = distribution.sumOf { it.probability }
            for (entry in distribution) {
                entry.probability /= totalProbability
            }
        }


    }


}
