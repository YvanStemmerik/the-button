package com.gnrlschnavy.thebutton.event

data class ButtonPressEvent(
    val pressed: Boolean
)
