package com.gnrlschnavy.thebutton.event

data class ButtonReleaseEvent(
    val released: Boolean
)
