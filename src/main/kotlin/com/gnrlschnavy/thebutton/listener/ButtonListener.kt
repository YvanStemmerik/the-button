package com.gnrlschnavy.thebutton.listener

import com.gnrlschnavy.thebutton.domain.Button
import com.gnrlschnavy.thebutton.event.ButtonPressEvent
import com.gnrlschnavy.thebutton.event.ButtonReleaseEvent
import com.gnrlschnavy.thebutton.service.ButtonPressesService
import com.gnrlschnavy.thebutton.service.DistributionService
import com.gnrlschnavy.thebutton.service.MessageService
import com.gnrlschnavy.thebutton.service.MusicService
import com.pi4j.io.gpio.digital.Digital
import com.pi4j.io.gpio.digital.DigitalStateChangeEvent
import com.pi4j.io.gpio.digital.DigitalStateChangeListener
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.ApplicationEventPublisher
import org.springframework.stereotype.Component
import java.time.LocalDateTime


@Component
class ButtonListener @Autowired constructor(
    private val messageService: MessageService,
    private val musicService: MusicService,
    private val buttonService: ButtonPressesService,
    private val applicationEventPublisher: ApplicationEventPublisher,
    private val distributionService: DistributionService,
) : DigitalStateChangeListener {

    override fun onDigitalStateChange(p0: DigitalStateChangeEvent<out Digital<*, *, *>>) {

        println("Start of onDigitalStateChange")

        if (p0.state().isHigh) {
            applicationEventPublisher.publishEvent(ButtonPressEvent(true))
            Button.totalCount++
            Button.timesPressedSinceLastMessage++
            if (Button.totalCount % distributionService.pickValueFromDistribution() == 0) {
                CoroutineScope(Dispatchers.IO).launch {
                    musicService.playNormalSound()
                }
            }
            messageService.determineMessage()
            buttonService.saveButtonPress()
            Button.timeLastPressed = LocalDateTime.now()
        } else {
            applicationEventPublisher.publishEvent(ButtonReleaseEvent(true))
            println("Button released!")
        }
    }


}
