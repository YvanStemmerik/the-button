package com.gnrlschnavy.thebutton.handler


import com.fasterxml.jackson.module.kotlin.jsonMapper
import com.fasterxml.jackson.module.kotlin.kotlinModule
import okhttp3.Headers
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.RequestBody.Companion.toRequestBody
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service


@Service
class OkRequestHandler {

    private val client = OkHttpClient()

    @Value("\${button.headerKey}")
    private var headerKey = ""

    @Value("\${button.headerSecret}")
    private var headerSecret = ""

    fun <T> post(
        url: String,
        requestParams: Map<String, String> = emptyMap(),
        requestBody: String,
        expectedReturnType: Class<T>,
    ): T {
        logger.info("Message send: $requestBody")
        val urlWithParams = createUrlWithParams(url, requestParams)
        val request = Request.Builder()
            .headers(setHeaders())
            .url(urlWithParams)
            .post(requestBody.toRequestBody())
            .build()

        client.newCall(request).execute().use { response ->
            with(response) {
                when (isSuccessful) {
                    true -> return mapper.readValue(response.body!!.string(), expectedReturnType)
                    false -> logger.info(response.body.toString())
                }
            }
            return mapper.readValue(response.body!!.string(), expectedReturnType)
        }
    }

    private fun createUrlWithParams(url: String, requestParams: Map<String, String>): String {
        return when {
            requestParams.isEmpty() -> url
            else -> {
                val params = mapParams(requestParams)
                "$url?$params"
            }
        }
    }

    private fun mapParams(requestParams: Map<String, String>?): String? {
        return requestParams?.map { "${it.key}=${it.value}" }?.joinToString("&")
    }


    private final fun setHeaders(): Headers =
        Headers.Builder().apply {
            this["Content-Type"] = "application/json"
            this["charset"] = "utf-8"
            this[headerKey] = headerSecret
        }.build()

    private val mapper = jsonMapper {
        addModule(kotlinModule())
    }
    val logger: Logger = LoggerFactory.getLogger(this::class.java)

}

