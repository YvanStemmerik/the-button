package com.gnrlschnavy.thebutton.domain

data class HighScoreDTO(
    val id: Long,
    val type: HighScoreType,
    val highScoreValue: Int
)


enum class HighScoreType{
    LONGPRESS, //0
    MANYPRESS, //1
}
