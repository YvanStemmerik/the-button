package com.gnrlschnavy.thebutton.domain

data class FileInfo(
    val directory: String,
    val filename: String
)
