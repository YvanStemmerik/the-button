package com.gnrlschnavy.thebutton.domain.jpa

import jakarta.persistence.Entity
import jakarta.persistence.GeneratedValue
import jakarta.persistence.GenerationType
import jakarta.persistence.Id
import java.time.LocalDateTime

@Entity
data class ButtonPresses (
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "button_presses_id_seq")
    val id: Long = -1,
    val dateTime: LocalDateTime? = LocalDateTime.now()
)
