package com.gnrlschnavy.thebutton.domain.jpa

import com.gnrlschnavy.thebutton.domain.HighScoreType
import jakarta.persistence.Entity
import jakarta.persistence.EnumType
import jakarta.persistence.Enumerated
import jakarta.persistence.GeneratedValue
import jakarta.persistence.GenerationType
import jakarta.persistence.Id

@Entity
class Highscore(
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "highscore_id_seq")
    val id: Long,
    @Enumerated(EnumType.ORDINAL)
    val type: HighScoreType,
    val highscoreValue: Int
)
