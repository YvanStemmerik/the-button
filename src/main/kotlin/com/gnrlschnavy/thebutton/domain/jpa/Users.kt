package com.gnrlschnavy.thebutton.domain.jpa

import jakarta.persistence.Entity
import jakarta.persistence.GeneratedValue
import jakarta.persistence.GenerationType
import jakarta.persistence.Id

@Entity
data class Users (
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "users_id_seq")
    val id: Long,
    val firstName: String,
    val phoneNumber: String
)
