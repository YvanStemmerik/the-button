package com.gnrlschnavy.thebutton.domain.jpa

import com.gnrlschnavy.thebutton.domain.MessageType
import jakarta.persistence.Entity
import jakarta.persistence.EnumType
import jakarta.persistence.Enumerated
import jakarta.persistence.GeneratedValue
import jakarta.persistence.GenerationType
import jakarta.persistence.Id
import java.time.LocalDateTime
import java.time.LocalTime

@Entity
data class Message (
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "message_id_seq")
    val id: Long,
    val text: String,
    @Enumerated(EnumType.ORDINAL)
    val type: MessageType,
    val receiverType: String,
    val notBefore: LocalTime?,
    val notAfter: LocalTime?,
    var timesSent: Int,
    val maxAmount: Int,
    val media: String?,
    val nextMessage: Long?,
    val exactDateTime: LocalDateTime?,
    val priority: Boolean
)
