package com.gnrlschnavy.thebutton.domain.jpa

import com.gnrlschnavy.thebutton.domain.MessageType
import jakarta.persistence.Entity
import jakarta.persistence.EnumType
import jakarta.persistence.Enumerated
import jakarta.persistence.GeneratedValue
import jakarta.persistence.GenerationType
import jakarta.persistence.Id
import java.time.LocalDateTime

@Entity
data class SendMessage (
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "send_message_id_seq")
    val id: Long,
    @Enumerated(EnumType.ORDINAL)
    val messageType: MessageType,
    val messageId: Long,
    val timeSend: LocalDateTime
)
