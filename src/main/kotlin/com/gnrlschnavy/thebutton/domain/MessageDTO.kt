package com.gnrlschnavy.thebutton.domain

import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper

data class MessageDTO(
    @JsonProperty("to_number")
    val toNumber: String,
    @JsonProperty("type")
    val type: String = "text",
    @JsonProperty("message")
    val message: String,
) {
    fun toJsonString(): String =
        jacksonObjectMapper().writeValueAsString(this)
}

enum class MessageType{
    PHOTO, //0
    SCHEDULED, //1
    AANKLEDING, //2
    STORY, //3
    ASSIGNMENT, //4
    MANYPRESSES, //5
    NOTPRESSEDINAWHILE, //6
    UNKNOWN, //7
    SECRETCODE, // 8
    LONGPRESS // 9
}

enum class MessageGroups{
    AANKLEDING
}
