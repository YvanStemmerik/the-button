package com.gnrlschnavy.thebutton.domain

import org.slf4j.LoggerFactory
import java.time.Duration
import java.time.LocalDateTime


object Button {

    private val logger = LoggerFactory.getLogger(this::class.java)

    var totalCount = 0
    var timeLastPressed = LocalDateTime.now()!!
    var timesPressedSinceLastMessage = 0
    var timeLastSendMessage = LocalDateTime.now()!!

    private  val ranges = mapOf(
        0..30 to 0.7001, // 1/1000
        31..45 to 0.02, // 1/50
        46..90 to 0.04, //  1/25
        91.. 120 to 0.1, // 1/10
        121.. 360 to 0.7, // 7/10
    )

    fun shouldSendMessage(): Boolean {
        val minutesPassed = Duration.between(timeLastSendMessage, LocalDateTime.now()).toMinutes()
        val chance: Double = ranges.entries.firstOrNull { minutesPassed in it.key }?.value ?: 0.5
        val random = Math.random() < chance
        logger.info("chance: $chance after $minutesPassed minutes passed, random: $random")
        return random
    }
}
