package com.gnrlschnavy.thebutton.domain

import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper

data class MediaMessageDTO(
    @JsonProperty("to_number")
    val toNumber: String,
    @JsonProperty("type")
    val type: String = "media",
    @JsonProperty("message")
    val media: String,
    @JsonProperty("text")
    val message: String? = ""
) {
    fun toJsonString(): String =
        jacksonObjectMapper().writeValueAsString(this)
}
