package com.gnrlschnavy.thebutton.domain

import com.gnrlschnavy.thebutton.domain.jpa.Users

class Group {
    fun getGroups(): Map<MessageGroups, List<Users>> {
        return mapOf(
            MessageGroups.AANKLEDING to listOf(
                Users(id = -1, phoneNumber =  "+31643083863", firstName = "SokAankleding"),
                Users(id = -1, phoneNumber =  "+31610042024", firstName = "YvanAankleding")
            )
        )
    }
}
