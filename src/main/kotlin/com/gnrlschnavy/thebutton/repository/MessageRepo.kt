package com.gnrlschnavy.thebutton.repository

import com.gnrlschnavy.thebutton.domain.MessageType
import com.gnrlschnavy.thebutton.domain.jpa.Message
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface MessageRepo: JpaRepository<Message, Long>{

    fun findMessageByType(type: MessageType): List<Message>

}

