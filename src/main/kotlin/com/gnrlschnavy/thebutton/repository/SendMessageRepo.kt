package com.gnrlschnavy.thebutton.repository

import com.gnrlschnavy.thebutton.domain.MessageType
import com.gnrlschnavy.thebutton.domain.jpa.SendMessage
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import java.time.LocalDateTime

@Repository
interface SendMessageRepo: JpaRepository<SendMessage, Long> {

    fun findAllByMessageTypeAndTimeSendAfter(notpressedinawhile: MessageType, after: LocalDateTime): SendMessage?
    fun findAllByTimeSendAfter(after: LocalDateTime): List<SendMessage>
}
