package com.gnrlschnavy.thebutton.repository

import com.gnrlschnavy.thebutton.domain.jpa.ButtonPresses
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import java.time.LocalDateTime

@Repository
interface ButtonPressesRepo : JpaRepository<ButtonPresses, Long> {

    fun countAllByDateTimeAfter(threshold: LocalDateTime = LocalDateTime.now().minusSeconds(10)): Long
    fun countAllBy(): Long
}
