package com.gnrlschnavy.thebutton.repository

import com.gnrlschnavy.thebutton.domain.HighScoreType
import com.gnrlschnavy.thebutton.domain.jpa.Highscore
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface HighScoreRepo: JpaRepository<Highscore, Long>{
        fun findAllByType(highScoreType: HighScoreType): List<Highscore>
}
