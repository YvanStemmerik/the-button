package com.gnrlschnavy.thebutton.repository

import com.gnrlschnavy.thebutton.domain.jpa.Users
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface UserRepo: JpaRepository<Users, Long>
