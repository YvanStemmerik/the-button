package com.gnrlschnavy.thebutton.controller

import com.gnrlschnavy.thebutton.domain.Button
import com.gnrlschnavy.thebutton.event.ButtonPressEvent
import com.gnrlschnavy.thebutton.event.ButtonReleaseEvent
import com.gnrlschnavy.thebutton.service.ButtonPressesService
import com.gnrlschnavy.thebutton.service.DistributionService
import com.gnrlschnavy.thebutton.service.MessageService
import com.gnrlschnavy.thebutton.service.MusicService
import org.springframework.context.ApplicationEventPublisher
import org.springframework.core.io.Resource
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import java.time.LocalDateTime


@RestController
@RequestMapping("/api/button")

class ButtonController(
    private val buttonPressService: ButtonPressesService,
    private val musicService: MusicService,
    private val messageService: MessageService,
    private val applicationEventPublisher: ApplicationEventPublisher,
    private val distributionService: DistributionService,
) {

    @GetMapping("/press")
    suspend fun pressButton(
    ): ResponseEntity<Resource?> {
        applicationEventPublisher.publishEvent(ButtonPressEvent(true))
        Button.totalCount++
        Button.timesPressedSinceLastMessage++
        val playSound = Button.totalCount % distributionService.pickValueFromDistribution() == 0
        messageService.determineMessage()
        buttonPressService.saveButtonPress()
        Button.timeLastPressed = LocalDateTime.now()

        applicationEventPublisher.publishEvent(ButtonReleaseEvent(true))
        return if(playSound) {
            musicService.streamRandomAudio("sounds" , 5)
        } else{
            ResponseEntity.ok().body(null)
        }
    }
}

