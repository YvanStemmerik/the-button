package com.gnrlschnavy.thebutton.controller

import com.gnrlschnavy.thebutton.service.MusicService
import org.springframework.core.io.Resource
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController


@RestController
@RequestMapping("/api/sound")

class SoundController(
    private val musicService: MusicService,
) {
    @GetMapping("/{folder}/{filename}")
    suspend fun pressButton(
        @PathVariable folder: String,
        @PathVariable filename: String,
    ): ResponseEntity<Resource?> {
        return musicService.streamSpecificAudio(folder, filename)
    }
}

