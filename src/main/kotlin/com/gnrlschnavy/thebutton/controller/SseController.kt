package com.gnrlschnavy.thebutton.controller

import com.gnrlschnavy.thebutton.service.SseService
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter

@RestController
class SseController(private val sseService: SseService) {

    @GetMapping("/api/sse", produces = [MediaType.TEXT_EVENT_STREAM_VALUE])
    fun streamEvents(): SseEmitter {
        val emitter = SseEmitter()
        sseService.addEmitter(emitter)
        emitter.onCompletion { sseService.removeEmitter(emitter) }
        emitter.onTimeout { sseService.removeEmitter(emitter) }
        return emitter
    }
}
