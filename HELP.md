Need to remembers
install gpio on pi:

```sudo apt-get install pigpio```

Need to do's
- Fix platform check for docker on windows
- Fix to send audio (when on docker or using web interface to actual webinterface instead of playing on native system)


For testing only 5 sounds are used.
If you want to use this in "production" move all files in resources/sounds/all to resources/sounds and change

```        playRandomSound("sounds", 5)``` and
```            musicService.streamRandomAudio("sounds" , 5)```
to
```        playRandomSound("sounds", 1550)``` and
```            musicService.streamRandomAudio("sounds" , 1550)```


We have two modes:

1. On a raspberry pi
2. Not on a raspberry pi

1. On a raspberry pi all button clicks are registered through GPIO pins and audio is played directly from the device
2. When not running on a raspberry pi a simple website with a button on root of the running application is shown
    Audio is streamed to the webpage and played.
