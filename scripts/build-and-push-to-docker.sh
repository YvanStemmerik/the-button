VERSION="$1"
docker buildx build -t yvanstemmerik/the-button:$VERSION -f Dockerfile-dev --platform=linux/amd64 .
docker push yvanstemmerik/the-button:$VERSION
#docker run -d -p 2001:8080 --name the-button --privileged -e SPRING_PROFILES_ACTIVE=dev yvanstemmerik/the-button:0.0
