#!/bin/bash
chmod +x build-and-push-to-docker.sh build-and-push-to-pi.sh
VERSION="0.0.3"
cd ..
mvn clean
mvn package -Dmaven.test.skip

./scripts/build-and-push-to-docker.sh "$VERSION"

if [ $? -eq 0 ]; then
    echo "build-and-push-to-docker.sh."
else
    echo "build-and-push-to-docker.sh failed."
    exit 1 # Exit master_script.sh if script1.sh fails
fi

# Run script2.sh
./scripts/build-and-push-to-pi.sh

# Check if script2.sh finished successfully
if [ $? -eq 0 ]; then
    echo "build-and-push-to-pi.sh executed successfully."
else
    echo "build-and-push-to-pi.sh failed."
    exit 1 # Exit master_script.sh if script2.sh fails
fi
